<?php
/**
 * Template for displaying search forms
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */
?>

<form class="searchform" id="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<fieldset>
		<input type="text" name="s" id="s" placeholder="<?php _t( 'Zoeken ...' ); ?>" autocomplete="off">

		<button id="searchsubmit" type="submit">
			<i class="fa fa-search" aria-hidden="true"></i>
		</button>
	</fieldset>
</form>
