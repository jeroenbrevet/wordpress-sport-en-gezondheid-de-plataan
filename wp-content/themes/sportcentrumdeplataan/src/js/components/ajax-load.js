/**
 * Ajax Loader Sports
 *
 * This component requires jQuery and Slick to function.
 * You can require it on the page using <div data-component="ajax-load"></div>
 */

define( [ 'jquery' ], function( $ ) {
	$( function() {
		$( '#ajax-holder' ).on(
			'click', '.js-loader',
			function( event ) {
				event.preventDefault();

				var $this = $( this );
				var $holder = $this.parents( '#ajax-holder' );

				var url = $this.attr( 'href' );

				$.ajax(
					{
						url: url,
						success: function( data ) {
							$( '.sports__loader', $holder ).remove();

							$( '> ul', $holder ).append( data );

							if ( $( '> ul .sports__loader', $holder ).length > 0 ) {
								$( '> ul .sports__loader', $holder ).insertAfter( $( '> ul', $holder ) );
							}
						}
					}
				);
			}
		);
	} );
} );