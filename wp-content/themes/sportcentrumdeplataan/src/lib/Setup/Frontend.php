<?php

namespace Custom_Theme\Setup;

/**
 * Class Frontend
 *
 * This class contains all setup data for the frontend
 *
 * @since      1.0
 *
 * @package    WordPress
 * @subpackage Custom_Theme\Setup
 */
class Frontend {
	/**
	 * Frontend constructor
	 *
	 * @since 1.0
	 */
	public function __construct() {
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_stylesheets' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'move_js_to_footer' ] );
		add_action( 'get_footer', [ $this, 'enqueue_javascripts' ] );
		add_filter( 'script_loader_tag', [ $this, 'add_requirejs_attributes' ], 99, 2 );
		add_action( 'wp_head', [ $this, 'wp_head' ] );
		add_action( 'init', [ $this, 'cleanup_head' ] );
		add_filter( 'style_loader_tag', [ $this, 'clean_style_tag' ] );
		add_filter( 'script_loader_tag', [ $this, 'clean_script_tag' ] );
		add_filter( 'body_class', [ $this, 'clean_body_class' ] );
		add_filter( 'get_bloginfo_rss', [ $this, 'remove_default_description' ] );

		/**
		 * Disable wp rocket caching when caching variable is set in URL
		 */
		if ( isset( $_GET['caching'] ) && $_GET['caching'] === 'false' ) {
			add_filter( 'do_rocket_generate_caching_files', '__return_false' );
		}

		/**
		 * Remove the WordPress version from RSS feeds
		 */
		add_filter( 'the_generator', '__return_false' );
	}

	/**
	 * Loads the theme stylesheets
	 * Additional stylesheets can be added in functions.php
	 *
	 * @internal This function uses the `wp_enqueue_scripts` action
	 * @link     https://codex.wordpress.org/Plugin_API/Action_Reference/wp_enqueue_scripts
	 *
	 * @since    1.0
	 *
	 * @return void
	 */
	public function enqueue_stylesheets() {
		$stylesheets = unserialize( STYLESHEETS );

		foreach ( $stylesheets as $name => $stylesheet ) {
			$source       = ( strpos( $stylesheet['src'], '//' ) === false ) ? get_theme_file_uri( $stylesheet['src'] ) : $stylesheet['src'];
			$dependencies = ( isset( $stylesheet['dependencies'] ) ) ? $stylesheet['dependencies'] : [];
			$version      = ( isset( $stylesheet['version'] ) ) ? $stylesheet['version'] : false;
			$media        = ( isset( $stylesheet['media'] ) ) ? $stylesheet['media'] : 'all';
			$enqueue      = ( isset( $stylesheet['enqueue'] ) ) ? $stylesheet['enqueue'] : true;

			if ( $enqueue ) {
				wp_enqueue_style( $name, $source, $dependencies, $version, $media );
			} else {
				wp_register_style( $name, $source, $dependencies, $version, $media );
			}
		}
	}

	/**
	 * Move all JavaScripts to the footer
	 *
	 * @internal This function uses the `wp_enqueue_scripts` action
	 * @link     https://codex.wordpress.org/Plugin_API/Action_Reference/wp_enqueue_scripts
	 *
	 * @since    4.0
	 *
	 * @return void
	 */
	public function move_js_to_footer() {
		remove_action( 'wp_head', 'wp_print_scripts' );
		remove_action( 'wp_head', 'wp_print_head_scripts', 9 );
		remove_action( 'wp_head', 'wp_enqueue_scripts', 1 );
	}

	/**
	 * Loads the theme javascripts
	 * Additional javascripts can be added in functions.php
	 *
	 * @internal This function uses the `get_footer` action
	 * @link     https://codex.wordpress.org/Plugin_API/Action_Reference/get_footer
	 *
	 * @since    1.0
	 *
	 * @return void
	 */
	public function enqueue_javascripts() {
		$javascripts = unserialize( JAVASCRIPTS );
		$additional  = [];
		$enqueues    = [];

		if ( GOOGLE_MAPS ) {
			$additional['google-maps'] = [
				'src'     => '//maps.googleapis.com/maps/api/js?key=' . get_field( 'google_api_key', 'option' ),
				'enqueue' => false,
			];

			$additional['google-maps-infobox'] = [
				'src'          => '/dist/js/vendor/maps/infobox.js',
				'dependencies' => [ 'google-maps' ],
				'enqueue'      => false,
			];

			$additional['maps'] = [
				'src'          => '/dist/js/vendor/maps/maps.js',
				'dependencies' => [ 'jquery', 'google-maps', 'google-maps-infobox' ],
				'enqueue'      => false,
			];
		}

		if ( TYPEKIT !== false ) {
			$additional['typekit'] = [
				'src' => '//use.typekit.net/' . TYPEKIT . '.js',
			];
		}

		$javascripts = $additional + $javascripts;

		foreach ( $javascripts as $name => $javascript ) {
			$source       = ( strpos( $javascript['src'], '//' ) === false ) ? get_theme_file_uri( $javascript['src'] ) : $javascript['src'];
			$dependencies = ( isset( $javascript['dependencies'] ) ) ? $javascript['dependencies'] : [];
			$version      = ( isset( $javascript['version'] ) ) ? $javascript['version'] : false;
			$in_footer    = ( isset( $javascript['in_footer'] ) ) ? $javascript['in_footer'] : true;
			$enqueue      = ( isset( $javascript['enqueue'] ) ) ? $javascript['enqueue'] : true;

			wp_register_script( $name, $source, $dependencies, $version, $in_footer );

			if ( $enqueue ) {
				$enqueues[] = $name;
			}

			if ( $name === 'require' ) {
				wp_localize_script( 'require', 'variables', [
					'base_url' => get_theme_file_uri( 'dist/js/vendor' ),
				] );
			}
		}

		/**
		 * Here you are able to add custom JavaScript files to the footer.
		 * You can do so by using this function: wp_enqueue_footer_script( $name )
		 *
		 * @since 4.0
		 *
		 * @param array $enqueues Array containing all enqueues
		 */
		$enqueues = apply_filters( 'wp_enqueue_footer_array', $enqueues );

		foreach ( $enqueues as $enqueue_name ) {
			wp_enqueue_script( $enqueue_name );
		}

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		if ( TYPEKIT !== false ) {
			wp_add_inline_script( 'typekit', 'try{Typekit.load({ async: true });}catch(e){}' );
		}
	}

	/**
	 * Add the RequireJS data-main attribute
	 *
	 * @since 4.0
	 *
	 * @param string $input  The actual HTML of the script tag
	 * @param string $handle The handle (id) of the scrip tag
	 *
	 * @return string The script tag including the data-main attribute
	 */
	public function add_requirejs_attributes( $input, $handle ) {
		if ( $handle !== 'require' ) {
			return $input;
		}

		return str_replace( ' src', ' data-main="' . get_template_directory_uri() . '/dist/js/app" src', $input );
	}

	/**
	 * Add additional meta tags to the head of the document
	 *
	 * @internal This function uses the `wp_head` action
	 * @link     https://codex.wordpress.org/Plugin_API/Action_Reference/wp_head
	 *
	 * @since    1.0
	 *
	 * @return void
	 */
	public function wp_head() {
		if ( ! is_admin() ) {
			echo '<meta charset="' . get_bloginfo( 'charset' ) . '" />';

			if ( RESPONSIVE === true ) {
				echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />';
			} else {
				echo '<meta name="viewport" content="width=1000" />';
			}

			if ( class_exists( 'acf' ) ) {
				if ( ( $header_favicon = get_field( 'header_favicon', 'option' ) ) != null ) {
					$favicon_sizes = [ 16, 32, 96, 192 ];

					foreach ( $favicon_sizes as $size ) {
						echo '<link rel="apple-touch-icon" sizes="' . $size . 'x' . $size . '" href="' . $header_favicon['sizes'][ 'favicon_' . $size ] . '" />';
						echo '<link rel="icon" type="image/png" sizes="' . $size . 'x' . $size . '" href="' . $header_favicon['sizes'][ 'favicon_' . $size ] . '" />';
					}
				}
			}
		}
	}

	/**
	 * Cleanup the head of the document
	 *
	 * @internal This function uses the `init` action
	 * @link     https://codex.wordpress.org/Plugin_API/Action_Reference/init
	 *
	 * @since    3.4.6
	 *
	 * @return void
	 */
	public function cleanup_head() {
		remove_action( 'wp_head', 'feed_links_extra', 3 );
		add_action( 'wp_head', 'ob_start', 1, 0 );
		add_action( 'wp_head', function () {
			$pattern = '/.*' . preg_quote( esc_url( get_feed_link( 'comments_' . get_default_feed() ) ), '/' ) . '.*[\r\n]+/';
			echo preg_replace( $pattern, '', ob_get_clean() );
		}, 3, 0 );
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'wlwmanifest_link' );
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );
		remove_action( 'wp_head', 'wp_generator' );
		remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );
		remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		add_filter( 'use_default_gallery_style', '__return_false' );
		add_filter( 'emoji_svg_url', '__return_false' );

		global $wp_widget_factory;

		if ( isset( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'] ) ) {
			remove_action( 'wp_head', [
				$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
				'recent_comments_style'
			] );
		}
	}

	/**
	 * Clean up the output of the <style> tags
	 *
	 * @internal This function uses the `style_loader_tag` filter
	 * @link     https://developer.wordpress.org/reference/hooks/style_loader_tag
	 *
	 * @since    3.4.6
	 *
	 * @param string $input The stylesheet line
	 *
	 * @return string The cleaned stylesheet line
	 */
	public function clean_style_tag( $input ) {
		preg_match_all( "!<link rel=\"stylesheet\"\s?(id=\"[^']+\")?\s+href=\"(.*)\" type=\"text/css\" media=\"(.*)\" />!", $input, $matches );

		if ( empty( $matches[2] ) ) {
			return $input;
		}

		// Only display media if it is meaningful
		$media = $matches[3][0] !== '' && $matches[3][0] !== 'all' ? ' media="' . $matches[3][0] . '"' : '';

		return '<link rel="stylesheet" href="' . $matches[2][0] . '"' . $media . '>' . "\n";
	}

	/**
	 * Clean up the output of the <script> tags
	 *
	 * @internal This function uses the `script_loader_tag` filter
	 * @link     https://developer.wordpress.org/reference/hooks/script_loader_tag/
	 *
	 * @since    3.4.6
	 *
	 * @param string $input The javascript line
	 *
	 * @return mixed
	 */
	public function clean_script_tag( $input ) {
		$input = str_replace( "type='text/javascript' ", '', $input );

		return str_replace( "'", '"', $input );
	}

	/**
	 * Cleans body classes
	 *
	 * @internal This function uses the `body_class` filter
	 * @link     https://codex.wordpress.org/Plugin_API/Filter_Reference/body_class
	 *
	 * @since    3.4.6
	 *
	 * @param array $classes Array containing all classes
	 *
	 * @return array Cleaned array containing the body classes
	 */
	public function clean_body_class( $classes ) {
		if ( is_single() || is_page() && ! is_front_page() ) {
			if ( ! in_array( basename( get_permalink() ), $classes ) ) {
				$classes[] = basename( get_permalink() );
			}
		}

		$home_id_class  = 'page-id-' . get_option( 'page_on_front' );
		$remove_classes = [
			'page-template-default',
			$home_id_class
		];
		$classes        = array_diff( $classes, $remove_classes );

		return $classes;
	}

	/**
	 * Don't return the default description in the RSS feed if it has not been changed
	 *
	 * @internal This function uses the `get_bloginfo_rss` filter
	 * @link     https://developer.wordpress.org/reference/hooks/get_bloginfo_rss
	 *
	 * @since    3.4.6
	 *
	 * @param string $bloginfo The RSS tagline
	 *
	 * @return string The cleaned RSS tagline
	 */
	public function remove_default_description( $bloginfo ) {
		$default_tagline = 'Just another WordPress site';

		return ( $bloginfo === $default_tagline ) ? '' : $bloginfo;
	}
}
