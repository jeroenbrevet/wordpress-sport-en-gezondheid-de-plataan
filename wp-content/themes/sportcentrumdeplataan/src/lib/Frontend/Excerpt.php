<?php

namespace Custom_Theme\Frontend;

/**
 * Class Excerpt
 *
 * Handles all excerpt related hooks
 *
 * @since      2.0
 *
 * @package    WordPress
 * @subpackage Custom_Theme\Frontend
 */
class Excerpt {
	/**
	 * Excerpt constructor
	 *
	 * @since 3.4.6
	 */
	public function __construct() {
		add_filter( 'excerpt_length', [ $this, 'excerpt_length' ], 999 );
		add_filter( 'excerpt_more', [ $this, 'excerpt_more' ] );
	}

	/**
	 * Adjusts the global excerpt length
	 * Makes use of the `EXCERPT_LENGTH` constant in `functions.php`
	 *
	 * @internal This function is hooked on the `excerpt_length` filter
	 * @link     https://codex.wordpress.org/Plugin_API/Filter_Reference/excerpt_length
	 *
	 * @since    2.0
	 *
	 * @return int The new length for the excerpts
	 */
	public function excerpt_length() {
		return EXCERPT_LENGTH;
	}

	/**
	 * Adjusts the global excerpt more symbol
	 * Makes use of the `EXCERPT_MORE` constant in the `functions.php`
	 *
	 * @internal This function is hooked on the `excerpt_more` filter
	 * @link     https://codex.wordpress.org/Plugin_API/Filter_Reference/excerpt_more
	 *
	 * @since    2.0
	 *
	 * @return string The new excerpt more symbol
	 */
	public function excerpt_more() {
		return EXCERPT_MORE;
	}
}
