<?php
/**
 * Template Name: Lesrooster Page
 * Template Post Type: post, page
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

get_header();
get_template_part( 'template-parts/content', 'banner' ); ?>
    <main role="main">
    <div class="article">
        <div class="container">
            <div class="article__content">
                <h1><?php the_title(); ?></h1>
                <?php
                // Start the loop.
                while ( have_posts() ) {
                    the_post();

                    // Include the page content template.
                    get_template_part( 'template-parts/content', get_post_type() );
                } ?>
            </div>
        </div>
    </div>
<?php
get_template_part( 'template-parts/content', 'timetable' );
get_template_part( 'template-parts/content', 'trial' );
get_template_part( 'template-parts/content', 'latest-home' );
get_footer();
