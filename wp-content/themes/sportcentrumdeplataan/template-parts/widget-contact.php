<?php echo $args['before_widget'];
if ( ( $title = get_field( 'widget_title', $widget_id ) ) && ! empty( $title ) ) {
	echo $args['before_title'] . apply_filters( 'widget_title', $title ) . $args['after_title'];
}

if ( ( $address = get_field( 'address', $widget_id ) ) && ! empty( $address ) ):?>
	<address>
		<?php echo $address; ?>
	</address>
<?php endif;

if ( ( $phone = get_field( 'phone', $widget_id ) ) && ! empty( $phone ) ):?>
	<a href="tel:<?php echo strip_phone_number( $phone ); ?>">
		<i class="fa fa-phone" aria-hidden="true"></i>

		<?php echo $phone; ?>
	</a><br>
<?php endif;

if ( ( $email = get_field( 'email', $widget_id ) ) && ! empty( $email ) ):?>
	<a href="mailto:<?php echo $email; ?>">
		<i class="fa fa-envelope-o" aria-hidden="true"></i>

		<?php echo $email; ?>
	</a>
<?php endif;
echo $args['after_widget']; ?>
