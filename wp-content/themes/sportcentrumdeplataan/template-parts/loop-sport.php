<?php
/**
 * The template used for displaying loop post
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
?>
<li class="col-sm-6 col-md-4">
	<?php if ( has_post_thumbnail() ): ?>
		<figure>
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'sport' ); ?>

				<span><?php the_title(); ?></span>
			</a>
		</figure>
	<?php endif; ?>
</li>