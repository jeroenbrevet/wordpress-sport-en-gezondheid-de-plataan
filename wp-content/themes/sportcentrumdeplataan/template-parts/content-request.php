<?php
/**
 * The template part for displaying request content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$image   = get_field( 'request_image' );
$title   = get_field( 'request_title' );
$content = get_field( 'request_content' );
$button  = get_field( 'request_button' );

if ( ! empty( $image ) || ! empty( $title ) || ! empty( $content ) || ! empty( $button ) ):?>
	<div class="request container">
		<div class="row">
			<?php if ( ! empty( $image ) ): ?>
				<div class="col-sm-6">
					<?php echo wp_get_attachment_image( $image, 'request' ); ?>
				</div>
			<?php endif;

			if ( ! empty( $title ) || ! empty( $content ) || ! empty( $button ) ):?>
				<div class="col-sm-6 request__content">
					<?php if ( ! empty( $title ) ): ?>
						<h2><?php echo $title; ?></h2>
					<?php endif;

					echo $content;

					if ( ! empty( $button ) ):?>
						<a href="<?php echo $button['url']; ?>" class="button"><?php echo $button['title']; ?></a>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>