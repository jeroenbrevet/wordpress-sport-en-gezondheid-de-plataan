<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 *
 * @link       http://codex.wordpress.org/Template_Hierarchy
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */

get_header();
get_template_part( 'template-parts/content', 'home-banner' );
get_template_part( 'template-parts/content', 'service' ); ?>
	<div class="about">
		<div class="container">
			<div class="row d-flex">
				<div class="<?php echo ( has_post_thumbnail() ) ? 'col-md-8' : 'col-sm-12'; ?>">
					<?php
					// Start the loop.
					while ( have_posts() ) {
						the_post();

						// Include the page content template.
						get_template_part( 'template-parts/content', 'page' );
					} ?>
				</div>

				<?php if ( has_post_thumbnail() ): ?>
					<div class="col-md-4 d-flex align-items-end">
						<?php the_post_thumbnail(); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php get_template_part( 'template-parts/content', 'latest-home' );
get_template_part( 'template-parts/content', 'request' );
get_template_part( 'template-parts/content', 'review' );
get_footer();
